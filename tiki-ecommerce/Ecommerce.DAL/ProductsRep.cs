﻿using Ecommerce.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecommerce.DAL
{
    using Ecommerce.Common.DAL;
    using Ecommerce.Common.Rsp;
    using Ecommerce.DAL.Models;
    using System.Linq;

    public class ProductsRep : GenericRep<EcommerceContext, Products>
    {
        #region -- Override --
        public override Products Read(int id)
        {
            var res = All.FirstOrDefault(p => p.ProductId == id);
            return res;
        }


        public int Remove(int id)
        {
            var m = base.All.FirstOrDefault(p => p.ProductId == id);
            m = base.Delete(m);
            return m.ProductId;
        }
        #endregion

        #region -- Methods --

        #endregion
    }
}
