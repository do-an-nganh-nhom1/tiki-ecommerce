﻿using System;
using System.Collections.Generic;

namespace Ecommerce.DAL.Models
{
    public partial class Customers
    {
        public Customers()
        {
            Order = new HashSet<Order>();
        }

        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime? DoB { get; set; }
        public bool Sex { get; set; }

        public virtual ICollection<Order> Order { get; set; }
    }
}
