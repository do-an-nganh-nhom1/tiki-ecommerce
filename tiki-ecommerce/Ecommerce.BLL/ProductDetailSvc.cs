﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecommerce.BLL
{
    using DAL;
    using DAL.Models;
    using Ecommerce.Common.BLL;
    using Ecommerce.Common.Req;
    using Ecommerce.Common.Rsp;

    public class ProductDetailSvc : GenericSvc<ProductDetailRep, ProductDetails>
    {
        #region -- Overrides --
        public override SingleRsp Read(int id)
        {
            var res = new SingleRsp();

            var m = _rep.Read(id);
            res.Data = m;

            return res;
        }

        public override int Remove(int id)
        {
            var res = new SingleRsp();

            var m = _rep.Remove(id);
            res.Data = m;

            return 0;
        }
        #endregion

        #region -- Method --
        public object GetProductByCategoryId_Linq(String keyword, int page, int size, int categoryId)
        {
            return _rep.GetProductByCategoryId_Linq(keyword, page, size, categoryId);
        }
        #endregion
    }
}
