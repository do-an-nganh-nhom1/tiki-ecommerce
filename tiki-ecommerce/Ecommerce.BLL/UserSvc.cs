﻿using Ecommerce.Common;
using Ecommerce.Common.Req;
using Ecommerce.Common.Rsp;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
namespace Ecommerce.BLL
{
    public interface IUserSvc
    {
        AuthenticateRsp Authenticate(AuthenticateReq model);
        IEnumerable<UserReq> GetAll();
        UserReq GetById(int id);
    }

    public class UserSvc : IUserSvc
    {
        private List<UserReq> _users = new List<UserReq>
        {
            new UserReq { Id = 1, FirstName = "Test", LastName = "User", Username = "test", Password = "test" }
        };

        private readonly AppSettings _appSettings;

        public UserSvc(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        public AuthenticateRsp Authenticate(AuthenticateReq a)
        {
            var user = _users.FirstOrDefault(x => x.Username == a.Username && x.Password == a.Password);

            // return null if user not found
            if (user == null) return null;

            // authentication successful so generate jwt token
            var token = generateJwtToken(user);

            return new AuthenticateRsp(user, token);
        }

        public IEnumerable<UserReq> GetAll()
        {
            return _users;
        }

        public UserReq GetById(int id)
        {
            return _users.FirstOrDefault(x => x.Id == id);
        }

        // helper methods

        private string generateJwtToken(UserReq user)
        {
            // generate token that is valid for 5 minutes
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddMinutes(5),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

    }
}
