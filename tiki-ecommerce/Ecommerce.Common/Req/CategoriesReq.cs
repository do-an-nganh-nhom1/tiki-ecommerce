﻿
namespace Ecommerce.Common.Req
{
    public class CategoriesReq
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
