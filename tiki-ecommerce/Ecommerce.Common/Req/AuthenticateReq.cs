﻿
using System.ComponentModel.DataAnnotations;

namespace Ecommerce.Common.Req
{
    public class AuthenticateReq
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
