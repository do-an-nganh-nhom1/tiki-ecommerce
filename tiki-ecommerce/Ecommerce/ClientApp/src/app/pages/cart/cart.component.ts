import { Component, OnInit } from '@angular/core';

declare var $;
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  public counter : number = 1; 

  cart: any = {
    data: [],
    quantity: 1
  }

  constructor() { }

  ngOnInit() {
  }

  updateQuantity(index, quantity) {
    if (quantity < 1){
      return
    }

    this.cart.data[index] = quantity
  }

  render() {
  }

  inc() {
    this.counter += 1;

    if (this.counter > 1) {
      $('.qty-decrease').removeClass('qty-disable')
    }
  }

  dec() {
    this.counter -= 1;

    if (this.counter <= 1) {
      this.counter = 1
      $('.qty-decrease').addClass('qty-disable')
    }
  }


  quantity() {

    // $('.qty-decrease').on('click', function (e) {
    //   e.preventDefault();
    //   var $this = $(this);
    //   var $input = $this.closest('div').find('input');
    //   var value = parseInt($input.val());

    //   if (value > 1) {
    //     value = value - 1;
    //   } else {
    //     value = 1;
    //   }

    //   $input.val(value);

    // });

    // $('.qty-increase').on('click', function (e) {
    //   e.preventDefault();
    //   var $this = $(this);
    //   var $input = $this.closest('div').find('input');
    //   var value = parseInt($input.val());

    //   if (value < 100) {
    //     value = value + 1;
        
    //     // console.log($('.qty-input').val())
    //   } else {
    //     value = 100;
    //   }

    //   $input.val(value);
    // });

    // if ($('.qty-input').val() > 1) {
    //   $('.qty-decrease').removeClass('qty-disable')
    // }
  }
}
