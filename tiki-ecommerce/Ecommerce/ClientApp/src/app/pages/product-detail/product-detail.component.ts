import { Component, OnInit } from '@angular/core';

declare var $;

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.slider-list').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 3,
      prevArrow: '.prev',
      nextArrow: '.next'
    });
  }

}
