import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'paginationjs'

declare var $;
@Component({
  selector: 'app-product-brand',
  templateUrl: './product-brand.component.html',
  styleUrls: ['./product-brand.component.scss']
})
export class ProductBrandComponent implements OnInit {
  public res: any;
  public lstCategoryName: [];
  public lstProduct: [];
  flag: string = "1";
  categoryId = "";
  public keywords: any;

  products: any = {
    data: [],
    totalRecord: 0,
    page: 0,
    size: 12,
    totalPages: 0
  }

  constructor(
    private http: HttpClient,
    @Inject('BASE_URL') baseUrl: string,
  ) { }

  ngOnInit() {
    $('.slick-banner').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      prevArrow: '.prev',
      nextArrow: '.next'
    });

    this.getProduct(1);

    // $('#pagination-container').pagination({
    //   dataSource: [1, 2, 3, 4, 5, 6, 7],
    //   pageSize: 5,
    //   showPrevious: true,
    //   showNext: true,
    //   callback: function (data, pagination) {
    //     // template method of yourself
    //     var html = simpleTemplating(data);
    //     $('#data-container').html(html);
    //   }
    // })

    // function simpleTemplating(data) {
    //   var html = '<ul class="page-style-item">';
    //   $.each(data, function (index, item) {
    //     html += '<li>' + item + '</li>';
    //   });
    //   html += '</ul>';
    //   return html;
    // }
  }

  //lay danh sach san pham theo ten cua loai san pham chon o phan loai
  getProduct(cPage) {
    let x = {
      page: cPage,
      size: 12,
      keyword: "",
      categoryId: 1
    }
    this.http.post('https://localhost:44308/' + 'api/ProductDetail/get-product-by-categoryName-linq', x).subscribe(result => {
      this.flag = "1";
      this.products = result;
      this.products = this.products.data;
      console.log(this.products);
    }, error => console.error(error));
  }

  searchNext() {
    if (this.flag == "1") {
      if (this.products.page < this.products.totalPages) {
        let nextPage = this.products.page + 1;
        let x = {
          page: nextPage,
          size: 12,
          keyword: "",
          categoryId: 1
        }
        this.http.post('https://localhost:44308/' + 'api/ProductDetail/get-product-by-categoryName-linq', x).subscribe(result => {
          this.products = result;
          this.products = this.products.data;
          console.log(this.products);
        }, error => console.error(error));
      }
      else {
        alert("Bạn đang ở trang cuối cùng!");
      }
    }
    else {
      if (this.products.page < this.products.totalPages) {
        let nextPage = this.products.page + 1;
        let x = {
          page: nextPage,
          size: 12,
          keyword: "",
          categoryId: 1
        }
        this.http.post('https://localhost:44308/' + 'api/ProductDetail/get-product-by-categoryName-linq', x).subscribe(result => {
          this.products = result;
          this.products = this.products.data;
          console.log(this.products);
        }, error => console.error(error));
      }
      else {
        alert("Bạn đang ở trang cuối cùng!");
      }
    }
  }

  searchPrevious() {
    if (this.products.page > 1) {
      let nextPage = this.products.page - 1;
      let x = {
        page: nextPage,
        size: 12,
        keyword: "",
        categoryId: 1
      }
      this.http.post('https://localhost:44308/' + 'api/ProductDetail/get-product-by-categoryName-linq', x).subscribe(result => {
        this.products = result;
        this.products = this.products.data;
        console.log(this.products);
      }, error => console.error(error));
    }
    else {
      alert("Bạn đang ở trang đầu tiên!");
    }
  }

}
