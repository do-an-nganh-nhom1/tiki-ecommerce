import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
declare var $;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  category: any = {
    data: []
  };
  checkHover = true;
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

  }

  ngOnInit() {
    // this.http.get('https://localhost:44308/api/Categories/get-all')
    //   .subscribe(result => {
    //     this.category = result
    //     this.category = this.category.data
    //     console.log(this.category)
    //   }, error => console.error(error))

      $('.menu-banner-slick').slick({
        speed: 600,
        autoplay: true,
        autoplaySpeed: 10000,
        infinite: true,
        slidesToScroll: 1,
        prevArrow: '.slick-btn-prev',
        nextArrow: '.slick-btn-next'
      });
  }

  

  // check2 = !this.check1;
}
