﻿using Ecommerce.BLL;
using Ecommerce.Common.Req;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class UsersController : ControllerBase
    {
        //public UsersController(IUserSvc userSvc)
        //{
        //    _svc = userSvc;
        //}
        private IUserSvc _userSvc;

        public UsersController(IUserSvc userSvc)
        {
            _userSvc = userSvc;
        }

        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateReq a)
        {
            var response = _userSvc.Authenticate(a);

            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(response);
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userSvc.GetAll();
            return Ok(users);
        }


        //private readonly UserSvc _svc;
    }
}